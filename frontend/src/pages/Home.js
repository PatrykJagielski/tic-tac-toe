import '../App.css';
import React, {useState} from 'react';
import { useNavigate } from "react-router-dom"

export default function Home() {

  const navigate = useNavigate();
  const [input, setInput] = useState('');

  const handleJoin = (event) => {
    navigate('/room/' + input + '/0/');
  }

  return (
    <div className="background">
      <section className="title">
        <h1>Tic Tac Toe</h1>
        <input className='input' value={input} onChange={(e)=>setInput(e.target.value)}></input>
      </section>
        <button className='button' onClick={handleJoin}>Join</button>
    </div>
  );
}