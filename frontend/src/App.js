import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import TicTacToe from './pages/TicTacToe';
import Home from './pages/Home';
function App() {
  return (
    <Router>
      <Routes>
        <Route exact path='/' element={<Home/>} />
        <Route path='/room/:id/:player/' element={<TicTacToe/>} />
      </Routes>
    </Router>
  );
}

export default App;
