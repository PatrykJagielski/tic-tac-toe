from collections import defaultdict
from email.policy import default
import json
from typing import List

from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse

app = FastAPI()

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <h1>WebSocket Chat</h1>
        <h2>Your ID: <span id="ws-id"></span></h2>
        <form action="" onsubmit="sendMessage(event)">
            <input type="text" id="messageText" autocomplete="off"/>
            <button>Send</button>
        </form>
        <ul id='messages'>
        </ul>
        <script>
            var client_id = Date.now()
            document.querySelector("#ws-id").textContent = client_id;
            var ws = new WebSocket(`ws://localhost:8000/ws/1/${client_id}`);
            ws.onmessage = function(event) {
                var messages = document.getElementById('messages')
                var message = document.createElement('li')
                var content = document.createTextNode(event.data)
                message.appendChild(content)
                messages.appendChild(message)
            };
            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""

class TicTacToe:
    def __init__(self):
        self.board = ['','','','','','','','','']
        self.round = 0
        self.winner = None
        self.draw = False
        self.next_move = 'X'

    def __repr__(self) -> str:
        return self.board

    def __str__(self) -> str:
        return self.board

    def make_move(self, id: int):
        if self.board[id] != '':
            return 1
        self.board[id] = ['X','O'][self.round % 2]
        self.round += 1
        if self.round % 2:
            self.next_move = 'O'
        else:
            self.next_move = 'X'

        self.is_winner()
        return 0
    
    def is_winner(self):
        winning_conditions = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ]

        for a, b, c in winning_conditions:
            if self.board[a] == self.board[b] and self.board[b] == self.board[c] and\
                    self.board[a] != '' and self.board[b] != '' and self.board[c] != '':
                self.winner = self.board[a]
                return True
        
    def toJSON(self):
        return json.dumps({'board': self.board, 'next': self.next_move, 'winner': self.winner})

game_boards = {}
class ConnectionManager:
    def __init__(self):
        self.active_connections: dict = defaultdict(list)

    async def connect(self, websocket: WebSocket, room_id: int):
        await websocket.accept()
        self.active_connections[room_id].append(websocket)
        if len(self.active_connections[room_id]) == 2:
            game_boards[room_id] = TicTacToe()

    def disconnect(self, websocket: WebSocket, room_id: int):
        self.active_connections[room_id].remove(websocket)

    async def broadcast(self, message: dict, room_id: int):
        tmp = game_boards.get(room_id).toJSON()
        for connection in self.active_connections[room_id]:
            await connection.send_text(tmp)


manager = ConnectionManager()


@app.get("/")
async def get():
    return HTMLResponse(html)


@app.websocket("/ws/{room_id}/{client_id}")
async def websocket_endpoint(websocket: WebSocket, room_id: int, client_id: int):
    await manager.connect(websocket, room_id)
    try:
        while True:
            data = await websocket.receive_json()
            id = int(data['message'])
            board = game_boards.get(room_id)
            if board:
                board.make_move(id)
            await manager.broadcast(data, room_id)
    except WebSocketDisconnect:
        manager.disconnect(websocket, room_id)
        await manager.broadcast(f"Client #{client_id} left the chat", room_id)