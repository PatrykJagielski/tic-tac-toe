import '../App.css';
import React, { useState, useEffect, useRef } from 'react';
import { useParams } from "react-router-dom";

export default function TicTacToe() {

  const [board, setBoard] = useState(['','','','','','','','','']);
  const [next, setNext] = useState('X')
  const [winner, setWinner] = useState(null)
  const ws = useRef(null);

  let params = useParams();

  useEffect(() => {
    ws.current = new WebSocket("wss://"+ process.env.REACT_APP_HOST +"/ws/" + params.id + "/" + params.player);
    ws.current.onopen = () => console.log("ws opened");
    ws.current.onclose = () => console.log("ws closed");
    ws.current.onmessage = e => {
      const message = JSON.parse(e.data);
      setBoard(message['board'])
      setNext(message['next'])
      setWinner(message['winner'])
    }

    const wsCurrent = ws.current;

    return () => {
        wsCurrent.close();
    };
  }, [params]);

  const sendMessage = (message) => {
    ws.current.send(JSON.stringify({
      'message': message
    }))
  };

  useEffect(() => {

  },[board])

  const handleClick = (event) => {
    sendMessage(event.target.id)
  }


  return (
    <div className="background">
      <section className="title">
        <h1>Tic Tac Toe</h1>
      </section>
      <section className="display">
        Player <span className={"display-player player" + (winner || next)}>{winner || next}</span>'s {winner ? 'wins' : 'turn'}
      </section>
      <div className="container">
        {board.map((o, i) => <div id={i} className={"tile player" + o} onClick={o === '' && winner == null ? handleClick : null}>{o}</div>)}
      </div>
    </div>
  );
}
